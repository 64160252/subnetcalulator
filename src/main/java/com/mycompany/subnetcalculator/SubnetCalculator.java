package com.mycompany.subnetcalculator;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class SubnetCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter IP Address: ");
        String ipAddress = scanner.next();

        System.out.print("Enter Subnet Mask: ");
        String subnetMask = scanner.next();

        try {
            InetAddress ip = InetAddress.getByName(ipAddress);
            InetAddress subnet = InetAddress.getByName(subnetMask);

            String networkAddress = calculateNetworkAddress(ip, subnet);
            String broadcastAddress = calculateBroadcastAddress(ip, subnet);
            String subnetMaskResult = calculateSubnetMask(subnet);
            int totalHosts = calculateTotalHosts(subnet);

            System.out.println("Network Address: " + networkAddress);
            System.out.println("Broadcast Address: " + broadcastAddress);
            System.out.println("Subnet Mask: " + subnetMaskResult);
            System.out.println("Total Hosts: " + totalHosts);
        } catch (UnknownHostException e) {
            System.err.println("Invalid IP address or subnet mask.");
        }
    }

    private static String calculateNetworkAddress(InetAddress ipAddress, InetAddress subnetMask) {
        byte[] ipBytes = ipAddress.getAddress();
        byte[] subnetBytes = subnetMask.getAddress();
        byte[] networkBytes = new byte[ipBytes.length];

        for (int i = 0; i < ipBytes.length; i++) {
            networkBytes[i] = (byte) (ipBytes[i] & subnetBytes[i]);
        }

        try {
            return InetAddress.getByAddress(networkBytes).getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String calculateBroadcastAddress(InetAddress ipAddress, InetAddress subnetMask) {
        byte[] ipBytes = ipAddress.getAddress();
        byte[] subnetBytes = subnetMask.getAddress();
        byte[] broadcastBytes = new byte[ipBytes.length];

        for (int i = 0; i < ipBytes.length; i++) {
            broadcastBytes[i] = (byte) (ipBytes[i] | ~subnetBytes[i]);
        }

        try {
            return InetAddress.getByAddress(broadcastBytes).getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String calculateSubnetMask(InetAddress subnetMask) {
        return subnetMask.getHostAddress();
    }

    private static int calculateTotalHosts(InetAddress subnetMask) {
        int subnetMaskInt = 0;
        byte[] subnetBytes = subnetMask.getAddress();
        for (byte b : subnetBytes) {
            subnetMaskInt = (subnetMaskInt << 8) + (b & 0xFF);
        }

        return ~subnetMaskInt - 1;
    }
}
